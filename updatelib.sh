#!/usr/bin/env bash

increment_version ()
{
  declare -a part=( ${1//\./ } )
  declare    new
  declare -i carry=1

  for (( CNTR=${#part[@]}-1; CNTR>=0; CNTR-=1 )); do
    len=${#part[CNTR]}
    new=$((part[CNTR]+carry))
    [ ${#new} -gt $len ] && carry=1 || carry=0
    [ $CNTR -gt 0 ] && part[CNTR]=${new: -len} || part[CNTR]=${new}
  done
  new="${part[*]}"
  echo -e "${new// /.}"
}



FILE=.libversion
if [ ! -f "$FILE" ]; then
    echo 0.0.1 >> .libversion
fi


VERSION=$(cat .libversion)

if [ -z $VERSION ]
  then
    echo "No argument supplied"
    exit 1
fi

echo "Bump to version ch.swissbytes.syscomapppayment:$VERSION"


./gradlew assembleRelease artifactoryPublish -PlibVersion=$VERSION

if [[ $? -eq 0 ]]; then
    echo $(increment_version $VERSION) > .libversion
    echo OUTPUT:
    echo "-----------------------------------------------------------
----------- ch.swissbytes.syscomapppayment:$VERSION -------------
-----------------------------------------------------------"
fi

