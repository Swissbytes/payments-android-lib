package ch.swissbytes.syscomapppayment.currency

import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomapppayment.base.extensions.realmValue
import ch.swissbytes.syscomapppayment.realm.RealmPayment
import ch.swissbytes.syscomapppayment.realm.entities.CurrencyRealm
import java.math.BigDecimal
import java.math.RoundingMode

class CurrencyService(val exchangeRate: BigDecimal) {

    fun USDtoBOB(number: BigDecimal): BigDecimal = number.multiply(this.exchangeRate).setScale(2, RoundingMode.HALF_UP)

    fun BOBtoUSD(number: BigDecimal?): BigDecimal {
        val aux = number ?: BigDecimal.ONE
        return aux divideDefaultRounding exchangeRate
    }

    infix fun BigDecimal.divideDefaultRounding(value: BigDecimal): BigDecimal = this.divide(value, 12, RoundingMode.HALF_UP)
    infix fun BigDecimal.divideNoRounding(value: BigDecimal): BigDecimal = this.divide(value, 10, RoundingMode.UP)

    fun BOBtoUSDNoRounding(number: BigDecimal?): BigDecimal {
        val aux = number ?: BigDecimal.ONE
        return aux divideNoRounding  exchangeRate
    }

}

fun getGlobalCurrencyService(onExchangeRateNotFound: GenericListener<Double>? = null):CurrencyService {
    val quote = realmValue(RealmPayment.getDefaultInstance()) { where(CurrencyRealm::class.java).findFirst()?.quote }
    return if (quote != null){
        CurrencyService(BigDecimal(quote)) //quote
    } else {
        val defaultValue = 6.89
        onExchangeRateNotFound?.invoke(defaultValue)
        CurrencyService(BigDecimal(defaultValue))
    }
}