package ch.swissbytes.syscomapppayment.base.binders

import android.widget.TextView
import androidx.databinding.BindingAdapter
import ch.swissbytes.syscomappbase.extensions.greaterOrEqualTo
import ch.swissbytes.syscomappbase.extensions.toFormatDecimalDefault
import ch.swissbytes.syscomappbase.extensions.toPositive
import ch.swissbytes.syscomappbase.views.NumberEditText
import ch.swissbytes.syscomapppayment.R
import java.math.BigDecimal


object ConvertorPayment {

    @JvmStatic @BindingAdapter("missingFormat") fun missingFormat(tv: TextView, value: BigDecimal) {
        val isPositive = value.greaterOrEqualTo(BigDecimal.ZERO)
        val format = tv.context.getString(if (isPositive) R.string.missing_format else R.string.exceed_format)
        tv.text = String.format(format, value.toPositive().toFormatDecimalDefault())
        tv.setTextColor(tv.resources.getColor(if (!isPositive) R.color.colorAccent else android.R.color.tab_indicator_text))
    }

    @JvmStatic @BindingAdapter("updateChange") fun updateChange(tv: NumberEditText, value: BigDecimal) {
        tv.runWithoutTextChangeWatcher {
            setText(value.toFormatDecimalDefault())
        }
    }

    private fun NumberEditText.runWithoutTextChangeWatcher(listener: NumberEditText.() -> Unit){
        removeTextChangedListener(textChangeWatcher)
        listener.invoke(this)
        addTextChangedListener(textChangeWatcher)
    }

}




