package ch.swissbytes.syscomapppayment.base.extensions

import ch.swissbytes.syscomappbase.extensions.greaterThanZero
import ch.swissbytes.syscomappbase.extensions.toBigDecimalOrZero


fun Long?.greaterThanZero() = toBigDecimalOrZero().greaterThanZero()