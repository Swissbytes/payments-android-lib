package ch.swissbytes.syscomapppayment.base.extensions

import io.realm.Realm
import io.realm.RealmModel

fun realmTransaction(realm: Realm = Realm.getDefaultInstance(), f: Realm.() -> Unit)
        = realmUse(realm) { executeTransaction { r -> f(r) } }

fun realmPersist(model: Any, realm: Realm = Realm.getDefaultInstance())
        = realmUse(realm) {
                when(model){
                    is RealmModel -> realmTransaction(realm) { copyToRealmOrUpdate(model) }
                    is List<*> -> realmTransaction(realm) { copyToRealmOrUpdate(model.checkRealmModelList()!!) }
                } }

fun realmUse(realm: Realm = Realm.getDefaultInstance(), f: Realm.() -> Unit) = realm.use { f(it) }
fun <T> realmValue(realm: Realm = Realm.getDefaultInstance(), f: Realm.() -> T) = realm.use { f(it) }



@Suppress("UNCHECKED_CAST")
fun List<*>.checkRealmModelList() = if (all { it is RealmModel }) this as List<RealmModel> else null