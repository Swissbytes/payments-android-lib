package ch.swissbytes.syscomapppayment.base.extensions

import android.util.Log
import ch.swissbytes.syscomappbase.extensions.greaterOrEqualTo
import ch.swissbytes.syscomappbase.extensions.greaterThan
import ch.swissbytes.syscomappbase.extensions.greaterThanZero
import ch.swissbytes.syscomappbase.extensions.toBigDecimalOrZero
import ch.swissbytes.syscomappbase.listeners.GenericListener
import java.math.BigDecimal

fun String?.greaterThan(value: BigDecimal) = toBigDecimalOrZero().greaterThan(value)

fun String?.greaterOrEqualTo(value: BigDecimal) = toBigDecimalOrZero().greaterOrEqualTo(value)

fun String?.greaterThanZero() = toBigDecimalOrZero().greaterThanZero()

fun CharSequence.parsePaymentInput(
    listener: GenericListener<String>
) {
    Log.i("TAG", "parsePaymentInput: $this")

    val countOfDecimal = count { it == '.' } > 1

    Log.w("TAG", "countOfDecimal: $countOfDecimal")

    var value = if (count { it == '.' } > 1) trimEnd { it == '.' } else this

    Log.w("TAG", "value after countOfDecimal: $value")

    value = if (value.toString() == ".") {
        "0."
    } else if (length > 1) {
        if (value.indexOfFirst { it == '0' } == 0 && value.indexOfFirst { it == '.' } != 1) {
            value.removePrefix("0")
        } else if (indexOfFirst { it == '.' } == 0) {
            value.removePrefix(".")
        } else value

    } else value

    Log.i("TAG", "parsePaymentInput output: $value")
    if (value.length > 9) value = value.subSequence(0, 9)

    listener.invoke(value.toString())
}

fun CharSequence.parsePaymentInputToBigDecimal(
    listener: GenericListener<BigDecimal>
) {

    val value = if (length > 1) removePrefix("0") else this

    listener.invoke(value.toString().removeSuffix(".").toBigDecimalOrZero())
}