package ch.swissbytes.syscomapppayment.fragments

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.DecelerateInterpolator
import android.view.animation.Transformation
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ch.swissbytes.dialogfilter.InkDialog
import ch.swissbytes.syscomappbase.extensions.*
import ch.swissbytes.syscomappbase.fragment.BaseViewStubFragment
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappbase.listeners.SimpleListener
import ch.swissbytes.syscomapppayment.AllPaymentsImpl
import ch.swissbytes.syscomapppayment.AllPaymentsInterface
import ch.swissbytes.syscomapppayment.BuildConfig
import ch.swissbytes.syscomapppayment.R
import ch.swissbytes.syscomapppayment.base.extensions.greaterThanZero
import ch.swissbytes.syscomapppayment.currency.CurrencyService
import ch.swissbytes.syscomapppayment.currency.getGlobalCurrencyService
import ch.swissbytes.syscomapppayment.fragments.payment.PaymentFragment
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentType
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.isLessThanDiffAllowed
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.isPartial
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.isValid
import ch.swissbytes.syscomapppayment.fragments.paymentadapter.PaymentsAdapter
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.fragment_payment_cash.*
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import kotlin.math.abs


/**
 * A fragment responsible to manage the [PaymentsAdapter] in case [isMultiPaymentAllowed]
 * @param totalPaymentUsd The total to pay can be change by [ch.swissbytes.mobilestore.activities.mobileorder.vm.MobileOrderVm] via [totalAmountToPayInUsd] property
 * @param paymentType
 *
 * @property onCurrentPaymentChange each time the payment change triggered from [PaymentFragment]
 * @property beforePaymentConfirm a callback allowing the calling class to reject the payment confirmation depending of conditions
 * @property totalAmountToPayInUsd set byt
 */
class PaymentTabFragment: BaseViewStubFragment() {

    private val TAG = this::class.java.simpleName

    interface OnPayment {
        fun onPaymentConfirm(signature: Bitmap?, payments: List<AllPaymentsInterface>?)
    }

    var onPaymentChange: GenericListener<Pair<BigDecimal, List<AllPaymentsInterface>>>? = null
    var beforePaymentConfirm: ((SimpleListener) -> Unit)? = null
    private val onPaymentChangeSubject by lazy { BehaviorSubject.create<AllPaymentsInterface>() }
    private lateinit var disposable: Disposable

    private val c: CurrencyService by lazy {
        getGlobalCurrencyService { defaultExchangeRate ->
            activity?.let {
                try {
                    it.showToast(it.getString(R.string.exchange_rate_not_found, defaultExchangeRate.toString()))
                } catch (e: Exception){
                    Log.e(TAG, "getGlobalCurrencyService defaultExchangeRate activity bad token")
                }
            }
        }
    }

    /**
     * The button which visibility state change on each [onCurrentPaymentChange] depending of the validity of the form displayed
     * Displayed at the bottom of the fragment instance in the layout XML
     */
    private var btnConfirm: TextView? = null

    /**
     * The fragment displayed
     */
    private var paymentFragment: PaymentFragment? = null

    /**
     * The parentView of the fragment created
     */
    private var contentView: View? = null

    /**
     * The paymentType selected
     */
    private lateinit var type: PaymentType

    private var isMultiPaymentAllowed: Boolean = true
    private var onPaymentConfirmed: OnPayment? = null
    private var isAdvance: Boolean = false

    private var screenWidthDp: Int = 520

    override fun getViewStubLayoutResource(): Int = R.layout.fragment_payment

    override fun delayBeforeHideProgress(): Long? = 400L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            type = PaymentType.valueOf(it.getString(ARG_PAYMENT_TYPE)!!)
            isMultiPaymentAllowed = it.getBoolean(ARG_MULTI_PAYMENT)
            totalAmountToPayInUsd = it.getString(ARG_PAYMENT_AMOUNT).toBigDecimalOrZero()
            isAdvance = it.getBoolean(ARG_IS_ADVANCE)
        }

        disposable = onPaymentChangeSubject.debounce(200, TimeUnit.MILLISECONDS).subscribe {
            activity?.runOnUiThread { onCurrentPaymentChange(it) }
        }
    }

    /**
     * Add the default (Cash) sub fragment lazily
     */
    override fun onCreateViewAfterViewStubInflated(view: View, savedInstanceState: Bundle?) {
        paymentFragment = PaymentFragment.newInstance(totalPaymentUsd = "$totalAmountToPayInUsd", isAdvance = isAdvance).apply {
            onPaymentChange = { onPaymentChangeSubject.onNext(it) }
            currencyUs = this@PaymentTabFragment.currencyUs
        }
        (activity as AppCompatActivity).addFragment(paymentFragment!!, R.id.fragment_p_container)
        onViewCreated(view)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }

    /**
     * Used as a public api
     */
    var totalAmountToPayInUsd: BigDecimal = BigDecimal.ZERO
        set(value) {
            field = value

            //update the total amount to pay in the paymentFragment in case
            val amountReceived = getCurrentAmountReceivedInUsd()
            val amountToPay = value.minus(amountReceived)
            paymentFragment?.totalAmountToPayInUsd = if (amountToPay.greaterThanZero()) amountToPay else BigDecimal.ZERO

            //Enable advance payment
            showConfirmPaymentBtn(amountReceived.greaterThanZero() && amountToPay.isLessThanDiffAllowed())
        }

    /**
     * Used as a public api
     */
    var currencyUs: Boolean? = null
        set(value) {
            field = value
            field?.let {
                paymentFragment?.currencyUs = it
            }
        }

    /**
     * Called each time the model of the showing payment fragment change
     * Set the value of [currPayment] locally
     * Check the [btnConfirm] visibility depending of the payment validity
     */
    private var currPayment: AllPaymentsInterface? = null
    private fun onCurrentPaymentChange(payment: AllPaymentsInterface) {
        currPayment = payment

        if (BuildConfig.DEBUG) Log.i(TAG, "currPayment change to: ${currPayment?.toPaymentImplementation()}")

        if (!isMultiPaymentAllowed){
            val currentAmountReceivedInBs = getCurrentAmountReceivedInBs()
            val paymentIsValid = payment.isValid(c, totalAmountToPayInUsd)
            val amountThresholdIsReach = if (!isAdvance) totalAmountToPayInUsd.greaterThanZero() else currentAmountReceivedInBs.greaterThanZero()
            showConfirmPaymentBtn(paymentIsValid && amountThresholdIsReach)
            onPaymentChange?.invoke(Pair(currentAmountReceivedInBs, listOf(payment)))
        } else {

            // The "add new payment" btn has to shown if the totalPaymentAmount is greater than zero
            // The confirmation top btn state while change on payment mPaymentsAdapter change
            enableAddPaymentBtn(enable = payment.toPaymentImplementation().isPartial())
        }
    }

    private fun enableAddPaymentBtn(enable: Boolean){
        mPaymentsAdapter.enableAddPaymentBtn(enable)
    }

    private fun addCurrentPaymentToAdapter(){
        addPaymentToAdapter(currPayment!!.toPaymentImplementation())
    }

    private fun AllPaymentsInterface.toPaymentImplementation() = AllPaymentsImpl(c, this)

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onPaymentConfirmed = context as OnPayment
    }

    private fun onViewCreated(view: View) {
        this.contentView = view
        setUpConfirmPaymentTopButton()
        setUpChangePaymentTypeBtn()

        if (isMultiPaymentAllowed){
            setUpRecyclerView()
        } else {
            view.findViewById<View>(R.id.rv)?.let { it.setBackgroundColor(ContextCompat.getColor(it.context, android.R.color.white)) }
            view.findViewById<View>(R.id.spacing)?.let { it.visibility = View.GONE }
        }
    }

    private fun setUpConfirmPaymentTopButton(){
        contentView?.apply{
            btnConfirm = findViewById(R.id.confirm)
            btnConfirm?.setOnClickListener {
                beforePaymentConfirm?.invoke { showSignatureDialog() } ?: showSignatureDialog()
            }
        }
    }

    private fun showSignatureDialog(){
        val dialog = InkDialog(activity as AppCompatActivity)
        dialog.showDialog {
            val payments = getListOfCurrentPayments()

            if (BuildConfig.DEBUG){
                Log.i(TAG, "payments:")
                payments.forEach {
                    Log.i(TAG, "amountReceivedBob: ${it.amountReceivedBob} differenceBob: ${it.differenceBob}")
                }
            }

            onPaymentConfirmed?.onPaymentConfirm(it, payments)
        }
    }

    private fun getListOfCurrentPayments() =
        (if (isMultiPaymentAllowed) mPaymentsAdapter.items else currPayment?.let { cur -> listOf(cur.toPaymentImplementation()) }) ?: emptyList()

    private fun getCurrentAmountReceivedInBs() =
        getListOfCurrentPayments().fold(BigDecimal.ZERO) { a, b ->  a.add(b.amountReceivedBob.toBigDecimalOrZero()) } ?: BigDecimal.ZERO

    private fun getCurrentAmountReceivedInUsd() =
        getListOfCurrentPayments().fold(BigDecimal.ZERO) { a, b ->  a.add(b.amountReceivedUsd.toBigDecimalOrZero()) } ?: BigDecimal.ZERO



    private fun setUpChangePaymentTypeBtn(){
        contentView?.apply {
            val btn = findViewById<TextView>(R.id.btn)
            btn.text = context.getString(type.res)

            btn.setOnClickListener {
                onChangePaymentTypeBtnClick(btn)
            }
        }
    }

    /**
     * Show the list of payments type
     */
    private fun onChangePaymentTypeBtnClick(btn: TextView) {
        val list = PaymentType.values()
        MaterialDialog(activity as Context).show {
            listItemsSingleChoice(items = list.map { context.getString(it.res) }) { _, index, _ ->
                val newType = list[index]
                if (newType != type){
                    paymentFragment = paymentFragment
                            ?.replaceFragmentPaymentType(newType, totalPaymentUsd = remainPaymentUsd)
                    type = newType
                    btn.text = context.getString(newType.res)
                }
                contentView?.findViewById<NestedScrollView>(R.id.sv)?.smoothScrollTo(0,0)
            }
        }
    }

    /**
     * The animation played when the become valid the reverse animation otherwise
     */
    private var isConfirmPaymentShowing = false
    private fun showConfirmPaymentBtn(show: Boolean){
        val animShouldRun = !((show && isConfirmPaymentShowing) || (!show && !isConfirmPaymentShowing))
        Log.i(TAG, "animShouldRun $animShouldRun show: $show")
        if (animShouldRun){
            object: Animation() {
                override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                    val value = if (show) (screenWidthDp * interpolatedTime).toInt()
                    else  (screenWidthDp * abs(interpolatedTime -1f)).toInt()

                    guideline?.setGuidelineEnd(value) }
            }.apply {
                interpolator = DecelerateInterpolator()
                fillAfter = true
                duration = if (show) 300L else 200L //in ms
                guideline?.startAnimation(this)
            }
        }
        isConfirmPaymentShowing = show
    }


    private fun addPaymentToAdapter(payment: AllPaymentsImpl?){
        payment?.let {
            if (it.amountReceivedBob.greaterThanZero()){
                mPaymentsAdapter.addItem(it)
                //reset the mPaymentsAdapter
                clearCurrentPaymentFragment()
            }
        }
    }

    var remainPaymentUsd: BigDecimal = BigDecimal.ZERO
        get() = totalAmountToPayInUsd.subtract(getMultiPaymentTotalReceivedAmountUsd()) ?: BigDecimal.ZERO

    private fun clearCurrentPaymentFragment(){
        paymentFragment?.onCurrentPaymentAddedToAdapter(remainPaymentUsd)
    }

    private val mPaymentsAdapter by lazy {
        PaymentsAdapter(context!!,
            onItemAdded =  {
                changeConfirmPaymentBtnState(it)
                onPaymentChange(it)
            },
            onItemRemoved =  {
                changeConfirmPaymentBtnState(it)
                paymentFragment?.onPaymentRemovedFromAdapter(remainPaymentUsd)
                onPaymentChange(it)
            }
        ) {
            addCurrentPaymentToAdapter()
        }
    }

    private fun onPaymentChange(payments: List<AllPaymentsInterface>){
        onPaymentChange?.invoke(Pair(getCurrentAmountReceivedInBs(), payments))
    }

    /**
     * @return BigDecimal.ZERO if no payment has been added to the multipayment mPaymentsAdapter list
     */
    private fun getMultiPaymentTotalReceivedAmountUsd() = c.BOBtoUSD(mPaymentsAdapter
            .items.fold(BigDecimal.ZERO) { a, b -> a.add(b.amountReceivedBob.toBigDecimalOrZero()) })

    private fun changeConfirmPaymentBtnState(payments: MutableList<AllPaymentsImpl>) {
        val total = payments.totalAmountReceivedBob()
        val totalPaymentBob = c.USDtoBOB(totalAmountToPayInUsd)
        val isValid = total.greaterThan(totalPaymentBob) || totalPaymentBob.subtract(total)?.isLessThanDiffAllowed() == true
        showConfirmPaymentBtn(isValid)
    }

    private fun setUpRecyclerView(){
        val rv = contentView?.findViewById<RecyclerView>(R.id.rv)
        rv?.apply {
            layoutManager = LinearLayoutManager(this@PaymentTabFragment.activity)
            adapter = this@PaymentTabFragment.mPaymentsAdapter
        }
    }

    private fun List<AllPaymentsImpl>.totalAmountReceivedBob() =
        this.fold(BigDecimal.ZERO) { a, b -> a.add(b.amountReceivedBob.toBigDecimalOrZero()) } ?: BigDecimal.ZERO

    private fun List<AllPaymentsImpl>.totalAmountReceivedUsd() =
        this.fold(BigDecimal.ZERO) { a, b -> a.add(b.amountReceivedUsd.toBigDecimalOrZero()) } ?: BigDecimal.ZERO

    companion object {
        val ARG_PAYMENT_AMOUNT = "ARG_PAYMENT_AMOUNT"
        val ARG_PAYMENT_TYPE = "ARG_PAYMENT_TYPE"
        val ARG_SHOULD_BE_SIGNED = "ARG_SHOULD_BE_SIGNED"
        val ARG_IS_ADVANCE = "ARG_IS_ADVANCE"
        val ARG_MULTI_PAYMENT = "ARG_MULTI_PAYMENT"
        @JvmStatic fun newInstance(
            type: PaymentType = PaymentType.CASH,
            totalAmountToPayInUsd: String = "0",
            isAdvance: Boolean = false,
            shouldBeSigned: Boolean = true,
            multiPayment: Boolean = true
        ) = PaymentTabFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_PAYMENT_AMOUNT, totalAmountToPayInUsd)
                putString(ARG_PAYMENT_TYPE, type.toString())
                putBoolean(ARG_IS_ADVANCE, isAdvance)
                putBoolean(ARG_SHOULD_BE_SIGNED, shouldBeSigned)
                putBoolean(ARG_MULTI_PAYMENT, multiPayment)
            } }
    }
}
