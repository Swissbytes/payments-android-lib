package ch.swissbytes.syscomapppayment.fragments.payment.vms.models

import androidx.annotation.StringRes
import ch.swissbytes.syscomapppayment.R


enum class PaymentCurrencies {
    BOB, USD
}

enum class PaymentType(@StringRes val res: Int) {
    CASH(R.string.pType_cash),
    CHECK(R.string.pType_check),
    ACCOUNT_DEPOSIT(R.string.pType_deposit),
    TRANSFER_ORDER(R.string.pType_tranference),
    DEBIT_CARD(R.string.pType_debit_card),
    CREDIT_CARD(R.string.pType_credit_card),
}