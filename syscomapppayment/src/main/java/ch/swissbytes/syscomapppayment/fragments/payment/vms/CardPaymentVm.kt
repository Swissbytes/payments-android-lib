package ch.swissbytes.syscomapppayment.fragments.payment.vms

import android.content.Context
import android.util.Log
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import ch.swissbytes.syscomappbase.extensions.greaterThanZero
import ch.swissbytes.syscomappbase.extensions.toBigDecimalOrZero
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomapppayment.AllPaymentsInterface
import ch.swissbytes.syscomapppayment.BR
import ch.swissbytes.syscomapppayment.base.extensions.parsePaymentInput
import ch.swissbytes.syscomapppayment.currency.getGlobalCurrencyService
import ch.swissbytes.syscomapppayment.fragments.payment.cashcalculation.UpdatePaymentListener
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentCurrencies
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentModel
import java.math.BigDecimal
import java.util.*

class CardPaymentVm(
    var context: Context,
    private var paymentM: PaymentModel,
    private var onPaymentChange: GenericListener<AllPaymentsInterface>
): Observer, BaseObservable(), UpdatePaymentListener {

    private val TAG = this::class.java.simpleName
    private val currencyService by lazy { getGlobalCurrencyService() }

    init {
        paymentM.addObserver(this)
        onPaymentChange.invoke(paymentM)
    }

    override fun update(o: Observable?, fieldChanged: Any?) {
        if (fieldChanged is String){
            when (fieldChanged){
                PaymentModel.BANK_ID -> notifyPropertyChanged(BR.currentBankSelected)
                PaymentModel.TRANSACTION_BOB -> notifyPropertyChanged(BR.transactionBob)
                PaymentModel.TRANSACTION_USD -> notifyPropertyChanged(BR.transactionUsd)
                PaymentModel.MISSING_BOB -> notifyPropertyChanged(BR.missingBob)
                PaymentModel.MISSING_USD -> notifyPropertyChanged(BR.missingUsd)
                PaymentModel.TRANSACTION_NUMBER -> notifyPropertyChanged(BR.transactionNumber)
                PaymentModel.AMOUNT_TO_PAY_IN_USD -> {
                    calculateMissingAmount(inputAmount.toBigDecimalOrZero())
                }
            }
            onPaymentChange.invoke(paymentM)
        }
    }

    fun clear(remainPaymentUsd: BigDecimal) {
        amountToPayInUsd = remainPaymentUsd
        inputAmount = BigDecimal.ZERO.toString()
        transactionNumber = ""
    }

    override var currency: PaymentCurrencies = PaymentCurrencies.BOB
    get() = PaymentCurrencies.valueOf(currencies[currenciesPosition])

    var inputAmount: String = BigDecimal.ZERO.toString() @Bindable get() = field
        set(value) {
            field = value
            updatePaymentTransactionAmount(value)
            notifyPropertyChanged(BR.inputAmount)
        }

    override var amountToPayInUsd: BigDecimal = BigDecimal.ZERO
        set(value) {
            field = value
            paymentM.amountToPayInUsd = value.toString()

        }

    var totalPaymentAmountBob: BigDecimal = BigDecimal.ZERO @Bindable get() = currencyService.USDtoBOB(amountToPayInUsd)

    private fun updatePaymentTransactionAmount(value : String){
        ifBOB({
            paymentM.transactionBob = value
            paymentM.transactionUsd = null
        }) {
            paymentM.transactionUsd = value
            paymentM.transactionBob = null
        }
        calculateMissingAmount(value.toBigDecimalOrZero())
    }

    private fun calculateMissingAmount(inputAmount : BigDecimal){
        ifBOB({
            missingBob = totalPaymentAmountBob.subtract(inputAmount).let { remain ->
                if (remain.greaterThanZero()) remain else BigDecimal.ZERO
            }
            Log.i(TAG, "missingBob $missingUsd")
        }) {
            missingUsd = amountToPayInUsd.subtract(inputAmount).let { remain ->
                if (remain.greaterThanZero()) remain else BigDecimal.ZERO
            }
            Log.i(TAG, "missingUsd $missingUsd")
        }
    }

    var missingBob: BigDecimal = BigDecimal.ZERO @Bindable get() = paymentM.missingBob.toBigDecimalOrZero()
        set(value) {
            field = value
            paymentM.missingBob = field.toString()
        }

    var missingUsd: BigDecimal = BigDecimal.ZERO @Bindable get() = paymentM.missingUsd.toBigDecimalOrZero()
        set(value) {
            field = value
            paymentM.missingUsd = field.toString()
        }

    fun <T> ifBOB(bob: () -> T, elseReturn: () -> T): T = if (currency == PaymentCurrencies.BOB) bob.invoke() else elseReturn.invoke()

    var currencies: List<String> = emptyList() @Bindable get() = PaymentCurrencies.values().map { it.name }

    var currenciesPosition = 0
        @Bindable get() = field
        set(value) {
            field = value;
            notifyPropertyChanged(BR.currenciesPosition)
            paymentM.currency = PaymentCurrencies.valueOf(currencies[value]).name
            updatePaymentTransactionAmount(inputAmount)
        }

    fun onTransactionAmountChange(s: CharSequence, start: Int, before: Int, count: Int) {
        Log.w(TAG, "onTotalPaymentAmountChange $s")
        s.parsePaymentInput {
            inputAmount = it
        }
    }


    var transactionNumber: String = "" @Bindable get() = paymentM.transactionNumber
        set(value) {
            field = value.replace("-", "")
            paymentM.transactionNumber = field
        }

}
