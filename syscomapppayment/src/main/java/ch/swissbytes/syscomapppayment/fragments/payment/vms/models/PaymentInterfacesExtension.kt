package ch.swissbytes.syscomapppayment.fragments.payment.vms.models

import ch.swissbytes.syscomappbase.extensions.greaterOrEqualTo
import ch.swissbytes.syscomappbase.extensions.greaterThan
import ch.swissbytes.syscomappbase.extensions.greaterThanZero
import ch.swissbytes.syscomappbase.extensions.toBigDecimalOrZero
import ch.swissbytes.syscomapppayment.AllPaymentsInterface
import ch.swissbytes.syscomapppayment.BuildConfig
import ch.swissbytes.syscomapppayment.base.extensions.greaterOrEqualTo
import ch.swissbytes.syscomapppayment.base.extensions.greaterThanZero
import ch.swissbytes.syscomapppayment.base.extensions.realmValue
import ch.swissbytes.syscomapppayment.currency.CurrencyService
import ch.swissbytes.syscomapppayment.realm.RealmPayment
import ch.swissbytes.syscomapppayment.realm.entities.PermittedDiffRealm
import java.math.BigDecimal
import java.util.*

fun getDiffAllowed() = realmValue(realm = RealmPayment.getDefaultInstance())
{ where(PermittedDiffRealm::class.java).findFirst()?.diffAllowed?.toBigDecimal() } ?: if (BuildConfig.DEBUG) BigDecimal.ONE else BigDecimal.ZERO

//@Deprecated("Has been set to Zero")
//internal fun getDiffAllowed() = BigDecimal.ZERO

//fun BigDecimal.isLessThanDiffAllowed() = !greaterThan(getDiffAllowed())
fun BigDecimal.isLessThanDiffAllowed() = !greaterOrEqualTo(getDiffAllowed())

fun AllPaymentsInterface.isUsd() = currency == PaymentCurrencies.USD.name
fun AllPaymentsInterface.hasValidTransactionNumber() = (transactionNumber?.length ?: 0) > 0
fun AllPaymentsInterface.hasValidVoucher() = (transactionNumber?.length ?: 0) > 0
fun AllPaymentsInterface.hasValidDate() = date?.let { Date(it).time > 0 } ?: false


/**
 * Check if any payment (BOB or USD) is greater than zero
 */
fun AllPaymentsInterface.hasValidCashPartialAmount() =
    amountReceivedBob.toBigDecimalOrZero().greaterThanZero()


fun AllPaymentsInterface.hasValidPartialAmount() = (if (isUsd()) transactionUsd else transactionBob).greaterThanZero()

fun AllPaymentsInterface.hasValidAmount(c: CurrencyService, toPayUsd: BigDecimal): Boolean {
    val amountUsd = if (isUsd()) transactionUsd!!.toBigDecimal() else c.BOBtoUSD(transactionBob.toBigDecimalOrZero())
    return amountUsd.greaterThan(toPayUsd) || toPayUsd.subtract(amountUsd)?.isLessThanDiffAllowed() == true
}

/**
 * Used to validate the payment validity in case of multipayment
 */
fun AllPaymentsInterface.isPartial(): Boolean =
    when (PaymentType.valueOf(type!!)){

        PaymentType.CASH ->
            hasValidCashPartialAmount()

        PaymentType.CREDIT_CARD, PaymentType.DEBIT_CARD ->
            hasValidPartialAmount()
                    && hasValidVoucher()

        PaymentType.ACCOUNT_DEPOSIT, PaymentType.CHECK, PaymentType.TRANSFER_ORDER -> {
            val a = hasValidDate()
            val b = hasValidTransactionNumber()
            val c = if (PaymentType.valueOf(type!!) == PaymentType.CHECK) bankId.greaterThanZero() else bankAccountId.greaterThanZero()
            val d =  hasValidPartialAmount()
            a && b && c && d
    }

}

/**
 * Used to validate the payment in case of single payment
 */
fun AllPaymentsInterface.isValid(c: CurrencyService, toPayUsd: BigDecimal): Boolean =

    when (val type = PaymentType.valueOf(type!!)){
        PaymentType.CASH ->
            !this.missingBob.greaterOrEqualTo(getDiffAllowed())

        PaymentType.CREDIT_CARD, PaymentType.DEBIT_CARD ->
            hasValidVoucher() && hasValidAmount(c, toPayUsd)


        PaymentType.ACCOUNT_DEPOSIT, PaymentType.CHECK, PaymentType.TRANSFER_ORDER ->
            hasValidDate()
                    && hasValidTransactionNumber()
                    && if (type == PaymentType.CHECK) bankId != null else bankAccountId != null
                    && hasValidAmount(c, toPayUsd)
    }


