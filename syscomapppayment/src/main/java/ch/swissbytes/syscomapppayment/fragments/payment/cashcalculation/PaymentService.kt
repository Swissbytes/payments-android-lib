package ch.swissbytes.syscomapppayment.fragments.payment.cashcalculation

import android.util.Log
import ch.swissbytes.syscomappbase.extensions.greaterOrEqualTo
import ch.swissbytes.syscomappbase.extensions.greaterThan
import ch.swissbytes.syscomapppayment.currency.CurrencyService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.util.*


class PaymentService(val c: CurrencyService) {

    private val TAG = this.javaClass.simpleName

    private val disposables = ArrayList<Disposable>()

    fun calculateCashFormValidityAsync(
        input: CashTransactionInput,
        allowedDifferenceCollection: BigDecimal,
        listener: (Boolean) -> Unit
    ) {


        val disposable = Observable.defer { Observable.just(calculateCashFormValidity(input, allowedDifferenceCollection))}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ listener.invoke(it) }) {
                //todo on error
            }
        disposables.add(disposable)
    }

    private fun calculateCashFormValidity(
        input: CashTransactionInput,
        allowedDifferenceCollection: BigDecimal
    ): Boolean {
        val totalReceived = getTotalReceived(input)
        return totalReceived greaterOrEqualTo input.amountToPayUsd.subtract(allowedDifferenceCollection)
    }

    fun getUsdRounding(input: CashTransactionInput): BigDecimal {
        val totalReceived = getTotalReceived(input)
        return totalReceived.subtract(input.amountToPayUsd)
    }

    private fun getTotalReceived(input: CashTransactionInput): BigDecimal {
        input.apply {
            val totalCashInBob = transactionBob.add(c.USDtoBOB(transactionUsd))
            val totalCashInUsd = c.BOBtoUSD(totalCashInBob)
            val totalChangeInBob = changeCashTransactionBob.add(c.USDtoBOB(changeCashTransactionUsd))
            val totalChangeInUsd = c.BOBtoUSD(totalChangeInBob)
            return totalCashInUsd.subtract(totalChangeInUsd)
        }
    }

    fun calculateChangeCashTransactionAsync(input: CashTransactionInput, listener: (CashTransactionOutput) -> Unit) {
        val disposable =  Observable.defer { Observable.just(calculateChangeCashTransaction(input)) }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                listener.invoke(it)
            }) {
                it.stackTrace.forEach { e -> Log.e(TAG, "calculateChangeCashTransactionAsync error: ${e.methodName}") }
            }

        disposables.add(disposable)
    }

    fun clear() {
        disposables.forEach { it.dispose() }
    }


    //In cash isUSD always true
    // amountToPayUsd is always USD
    private fun calculateChangeCashTransaction(input: CashTransactionInput): CashTransactionOutput {
        input.apply {
            val amountReceivedInBob = transactionBob.add(c.USDtoBOB(transactionUsd))
            val amountReceivedInUsd = c.BOBtoUSD(amountReceivedInBob)
            val totalChangeInBob = changeCashTransactionBob.add(c.USDtoBOB(changeCashTransactionUsd))
//            val totalChangeInUsd = BOBtoUSD(totalChangeInBob)

            val output = CashTransactionOutput()
            output.triggerByCashTransactionInputChange = input.triggerByCashTransactionInputChange




            if (amountReceivedInUsd greaterThan amountToPayUsd) {
                output.requiredChangeCashUsd = amountReceivedInUsd.subtract(amountToPayUsd)
                val amountToPayBob = c.USDtoBOB(amountToPayUsd)
                output.requiredChangeCashBob = amountReceivedInBob.subtract(amountToPayBob)

                output.missingChangeBob = if (input.isAdvance) BigDecimal.ZERO else output.requiredChangeCashBob.subtract(totalChangeInBob)
                output.missingChangeUsd = c.BOBtoUSD(output.missingChangeBob)

                output.changeCashTransactionBob = changeCashTransactionBob
                output.changeCashTransactionUsd = changeCashTransactionUsd
            } else {
                output.missingUsd = amountToPayUsd.subtract(amountReceivedInUsd)
                output.missingBob = c.USDtoBOB(output.missingUsd)
                output.changeCashTransactionBob = BigDecimal.ZERO
                output.changeCashTransactionUsd = BigDecimal.ZERO
                output.requiredChangeCashBob = BigDecimal.ZERO
                output.requiredChangeCashUsd = BigDecimal.ZERO
            }

            output.amountReceivedInBob = amountReceivedInBob
            return output
        }
    }
}
