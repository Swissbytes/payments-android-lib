package ch.swissbytes.syscomapppayment.fragments.payment.cashcalculation


import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentCurrencies
import java.math.BigDecimal

interface UpdatePaymentListener{
    var amountToPayInUsd: BigDecimal
    var currency: PaymentCurrencies
}