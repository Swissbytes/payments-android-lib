package ch.swissbytes.syscomapppayment.fragments.payment.vms

import android.content.Context
import android.util.Log
import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import ch.swissbytes.syscomappbase.extensions.greaterThanZero
import ch.swissbytes.syscomappbase.extensions.toBigDecimalOrZero
import ch.swissbytes.syscomappbase.extensions.toShortDateString
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappbase.views.DatePickerWrapper
import ch.swissbytes.syscomapppayment.AllPaymentsInterface
import ch.swissbytes.syscomapppayment.BR
import ch.swissbytes.syscomapppayment.R
import ch.swissbytes.syscomapppayment.base.extensions.parsePaymentInput
import ch.swissbytes.syscomapppayment.base.extensions.realmUse
import ch.swissbytes.syscomapppayment.currency.getGlobalCurrencyService
import ch.swissbytes.syscomapppayment.fragments.payment.cashcalculation.UpdatePaymentListener
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentCurrencies
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentModel
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentType
import ch.swissbytes.syscomapppayment.realm.RealmPayment
import ch.swissbytes.syscomapppayment.realm.entities.BankAccountRealm
import ch.swissbytes.syscomapppayment.realm.entities.BankInterface
import ch.swissbytes.syscomapppayment.realm.entities.BankRealm
import ch.swissbytes.syscomapppayment.realm.entities.EntityStatus
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import io.realm.Case
import java.math.BigDecimal
import java.util.*


class DepositPaymentVm(
    var context: Context,
    var paymentM: PaymentModel,
    var onPaymentChange: GenericListener<AllPaymentsInterface>
): Observer, BaseObservable(), UpdatePaymentListener {

    private val TAG = this::class.java.simpleName
    private var listOfBanks: List<BankInterface>? = null

    private val currencyService by lazy { getGlobalCurrencyService() }

    private val mustSelectBank by lazy { paymentM.type == PaymentType.CHECK.name }

    init {
        paymentM.addObserver(this)
        realmUse(RealmPayment.getDefaultInstance()) {
            val list = if (mustSelectBank) where(BankRealm::class.java).findAll()
            else where(BankAccountRealm::class.java).contains("status", EntityStatus.ENABLE.name, Case.INSENSITIVE).findAll()

            listOfBanks = if (list != null) copyFromRealm(list).map { it as BankInterface } else emptyList()
        }
        onPaymentChange.invoke(paymentM)
    }

    override fun update(o: Observable?, fieldChanged: Any?) {
        if (fieldChanged is String){
            when (fieldChanged){
                PaymentModel.DATE -> notifyPropertyChanged(BR.date)
                PaymentModel.BANK_NAME -> {
                    notifyPropertyChanged(BR.currentBankSelected)
                    notifyPropertyChanged(BR.currentBankSelectedIsValid)
                }
                PaymentModel.TRANSACTION_USD -> notifyPropertyChanged(BR.transactionUsd)
                PaymentModel.TRANSACTION_BOB -> notifyPropertyChanged(BR.transactionBob)
                PaymentModel.MISSING_BOB -> notifyPropertyChanged(BR.missingBob)
                PaymentModel.MISSING_USD -> notifyPropertyChanged(BR.missingUsd)
                PaymentModel.TRANSACTION_NUMBER -> notifyPropertyChanged(BR.transactionNumber)
                PaymentModel.AMOUNT_TO_PAY_IN_USD -> {
                    calculateMissingAmount(inputAmount.toBigDecimalOrZero())
                }
            }
            onPaymentChange.invoke(paymentM)
        }
    }

    fun clear(remainPaymentUsd: BigDecimal) {
        currentBankSelected = ""
        date = ""
        amountToPayInUsd = remainPaymentUsd
        inputAmount = BigDecimal.ZERO.toString()
        transactionNumber = ""
    }

    var type: PaymentType? = null
        get() = PaymentType.valueOf(paymentM.type!!)
        set(value) {
            field = value
            paymentM.type = value?.name
        }

    override var currency: PaymentCurrencies = PaymentCurrencies.BOB
    get() = PaymentCurrencies.valueOf(currencies[currenciesPosition])

    var currentBankSelected: String = "" @Bindable get() =
        paymentM.bankName ?: context.getString(if (type == PaymentType.CHECK) R.string.select_bank else R.string.select_bank_account)

    var currentBankSelectedIsValid: Boolean = false @Bindable get() = paymentM.bankName != null

    var date: String = "" @Bindable get() = paymentM.date?.let { Date(it).toShortDateString() } ?: ""
    set(value) {
        field = value
        if (value.isBlank()) paymentM.date = null
    }

    var inputAmount: String = BigDecimal.ZERO.toString() @Bindable get() = field
        set(value) {
            field = value
            updatePaymentTransactionAmount(value)
            notifyPropertyChanged(BR.inputAmount)
        }

    private fun updatePaymentTransactionAmount(value : String){
        ifBOB({
            paymentM.transactionBob = value
            paymentM.transactionUsd = null
        }) {
            paymentM.transactionUsd = value
            paymentM.transactionBob = null
        }
        calculateMissingAmount(value.toBigDecimalOrZero())
    }

    private fun calculateMissingAmount(inputAmount : BigDecimal){
        ifBOB({
            missingBob = totalPaymentAmountBob.subtract(inputAmount)
                .let { remain ->
                    if (remain.greaterThanZero()) remain else BigDecimal.ZERO
                }

            Log.i(TAG, "missingBob $missingUsd")
        }) {
            missingUsd = amountToPayInUsd.subtract(inputAmount)
                .let { remain ->
                    if (remain.greaterThanZero()) remain else BigDecimal.ZERO
                }
            Log.i(TAG, "missingUsd $missingUsd")
        }
    }

    var currencies: List<String> = emptyList() @Bindable get() = PaymentCurrencies.values().map { it.name }

    var currenciesPosition = 0
        @Bindable get() = field
        set(value) {
            field = value;
            notifyPropertyChanged(BR.currenciesPosition)
            paymentM.currency = PaymentCurrencies.valueOf(currencies[value]).name

            updatePaymentTransactionAmount(inputAmount)
            clearBankSelection()
        }

    private fun clearBankSelection(){
        if (mustSelectBank) paymentM.bankId = 0
        else paymentM.bankAccountId = 0

        paymentM.bankName = null
    }

    override var amountToPayInUsd: BigDecimal = BigDecimal.ZERO @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.amountToPayInUsd)
            notifyPropertyChanged(BR.missingUsd)
            paymentM.amountToPayInUsd = value.toString()
        }

    var totalPaymentAmountBob: BigDecimal = BigDecimal.ZERO
        @Bindable get() = currencyService.USDtoBOB(amountToPayInUsd)

    var missingBob: BigDecimal = BigDecimal.ZERO @Bindable get() = paymentM.missingBob.toBigDecimalOrZero()
        set(value) {
            field = value
            paymentM.missingBob = field.toString()
        }

    var missingUsd: BigDecimal = BigDecimal.ZERO @Bindable get() = paymentM.missingUsd.toBigDecimalOrZero()
        set(value) {
            field = value
            paymentM.missingUsd = field.toString()
        }

    fun onTransactionAmountChange(s: CharSequence, start: Int, before: Int, count: Int) {
        Log.w(TAG, "onTotalPaymentAmountChange $s")
        s.parsePaymentInput { value ->
            inputAmount = value
        }
    }

    private fun <T> ifBOB(bob: () -> T, elseReturn: () -> T): T =
        if (currency == PaymentCurrencies.BOB) bob.invoke() else elseReturn.invoke()


    var transactionNumber: String = "" @Bindable get() = paymentM.transactionNumber
        set(value) {
            field = value
            paymentM.transactionNumber = field
        }

    fun onBankBtnClick(v: View){

        val localListOfBank = if (mustSelectBank)
                listOfBanks
                ?.filterIsInstance(BankRealm::class.java)
                ?.filter { !it.bankName.isNullOrBlank() }
            else listOfBanks
                ?.filterIsInstance(BankAccountRealm::class.java)
                ?.filter { !it.bankName.isNullOrBlank() && !it.number.isNullOrBlank() && !it.currency.isNullOrBlank() }
                ?.filter { it.currency == currency.name }

        val listOfNames = if (mustSelectBank)
            localListOfBank
                ?.filterIsInstance(BankRealm::class.java)
                ?.map { it.bankName!!}
        else localListOfBank
            ?.filterIsInstance(BankAccountRealm::class.java)
            ?.map { "${it.number!!} - ${it.bankName!!} - ${it.currency!!}" }

        MaterialDialog(context).show {
            listItemsSingleChoice(items = listOfNames) { _, index, _ ->
                localListOfBank?.get(index)?.let {

                    if (mustSelectBank) paymentM.bankId = it.id
                    else paymentM.bankAccountId = it.id

                    paymentM.bankName = it.bankName
                }
                Log.i(TAG, "list: ${listOfNames?.get(index)}")
            }
        }
    }

    fun onDateInputClick(v: View){
        Log.i(TAG, "onDateInputClick")
        DatePickerWrapper.newInstance(context, R.style.DatePicker).apply {
            listener = { paymentM.date = it.time }
            show()
        }
    }

}
