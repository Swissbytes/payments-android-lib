package ch.swissbytes.syscomapppayment.fragments.payment.cashcalculation

import java.math.BigDecimal

data class CashTransactionOutput(
    var triggerByCashTransactionInputChange: Boolean = false,
    var amountReceivedInBob: BigDecimal = BigDecimal.ZERO,
    var changeCashTransactionBob: BigDecimal? = null,
    var changeCashTransactionUsd: BigDecimal? = null,
    var missingBob: BigDecimal = BigDecimal.ZERO,
    var missingUsd: BigDecimal = BigDecimal.ZERO,
    var missingChangeBob: BigDecimal = BigDecimal.ZERO,
    var missingChangeUsd: BigDecimal = BigDecimal.ZERO,
    var requiredChangeCashBob: BigDecimal = BigDecimal.ZERO,
    var requiredChangeCashUsd: BigDecimal = BigDecimal.ZERO
) {
    override fun toString(): String =
        "CashTransactionOutput(triggerByCashTransactionInputChange=$triggerByCashTransactionInputChange, " +
                "amountReceivedInBob=$amountReceivedInBob, " +
                "changeCashTransactionBob=$changeCashTransactionBob, " +
                "changeCashTransactionUsd=$changeCashTransactionUsd, " +
                "missingBob=$missingBob, " +
                "missingUsd=$missingUsd, " +
                "missingChangeBob=$missingChangeBob, " +
                "missingChangeUsd=$missingChangeUsd, " +
                "requiredChangeCashBob=$requiredChangeCashBob, " +
                "requiredChangeCashUsd=$requiredChangeCashUsd)"

}


data class CashTransactionInput(
    var triggerByCashTransactionInputChange: Boolean = false,
    var transactionBob: BigDecimal = BigDecimal.ZERO,
    var transactionUsd: BigDecimal = BigDecimal.ZERO,
    var changeCashTransactionBob: BigDecimal = BigDecimal.ZERO,
    var changeCashTransactionUsd: BigDecimal = BigDecimal.ZERO,
    var amountToPayUsd: BigDecimal = BigDecimal.ZERO, //ALWAYS USD
    var isAdvance: Boolean = false
)
