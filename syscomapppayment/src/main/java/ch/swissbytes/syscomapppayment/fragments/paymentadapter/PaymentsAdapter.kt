package ch.swissbytes.syscomapppayment.fragments.paymentadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ch.swissbytes.syscomappbase.extensions.toBigDecimalOrZero
import ch.swissbytes.syscomappbase.extensions.toFormatDecimalDefault
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappbase.listeners.SimpleListener
import ch.swissbytes.syscomapppayment.AllPaymentsImpl
import ch.swissbytes.syscomapppayment.R
import ch.swissbytes.syscomapppayment.base.extensions.greaterThan
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentCurrencies
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentType
import java.math.BigDecimal

/**
 * The first row in the adapter is the add payment Btn
 */
class PaymentsAdapter(
    val context: Context,
    val items: MutableList<AllPaymentsImpl> = mutableListOf(),
    private val onItemAdded: GenericListener<MutableList<AllPaymentsImpl>>,
    private val onItemRemoved: GenericListener<MutableList<AllPaymentsImpl>>,
    private val onAppPaymentClick: SimpleListener
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var enableAddPayment = false

    override fun getItemCount(): Int = items.size + 1

    override fun getItemViewType(position: Int): Int = if (position == 0) 0 else 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType){
            0 -> HeaderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.header_card, parent, false))
            else -> PaymentViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cards, parent, false))
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is HeaderViewHolder -> headerView(holder)
            is PaymentViewHolder -> paymentView(holder)
        }
    }

    private fun paymentView(holder: PaymentViewHolder){
        val item = items[holder.adapterPosition - 1]
        with(holder){
            t1.text = context.getString(PaymentType.valueOf(item.type!!).res)

            t2.text = context.getString(R.string.format_currency,
                item.currency?.let { item.currency }
                ?: if (item.transactionBob.greaterThanZero() && item.transactionUsd.greaterThanZero())
                    "${PaymentCurrencies.BOB.name} / ${PaymentCurrencies.USD.name}"
                    else if (item.transactionBob.greaterThanZero()) PaymentCurrencies.BOB.name
                    else if (item.transactionUsd.greaterThanZero()) PaymentCurrencies.USD.name
                    else ""
            )

            t3.text = context.getString(R.string.format_bob_and_usd,
                item.amountReceivedUsd,
                item.amountReceivedBob)
            trash.setOnClickListener { removeItem(holder.adapterPosition) }
        }
    }

    fun String?.greaterThanZero() = !isNullOrBlank() && this.greaterThan(BigDecimal.ZERO)

    private fun headerView(holder: HeaderViewHolder){
        with(holder){


            val totalAmountReceivedBob = items.fold(BigDecimal.ZERO) { a, b -> a.add(b.amountReceivedBob.toBigDecimalOrZero()) }
            val totalAmountReceivedUsd = items.fold(BigDecimal.ZERO) { a, b -> a.add(b.amountReceivedUsd.toBigDecimalOrZero()) }

            t2.text = context.getString(R.string.format_bob_and_usd,
                totalAmountReceivedUsd.toFormatDecimalDefault(),
                totalAmountReceivedBob.toFormatDecimalDefault())



            btn?.apply {
                visibility = if (enableAddPayment) View.VISIBLE else View.GONE
                if (enableAddPayment) setOnClickListener { onAppPaymentClick.invoke() }  else setOnClickListener(null)
            }
        }
    }

    fun addItem(item: AllPaymentsImpl){
        items.add(0, item)
        notifyItemInserted(1)
//        notifyItemChanged(0) // removed because called by [PaymentsAdapter.enableAddPaymentBtn]
        onItemAdded.invoke(items)
    }

    fun removeItem(position: Int){
        items.removeAt(position - 1)
        notifyItemRemoved(position)
        notifyItemChanged(0)
        onItemRemoved.invoke(items)
    }

    fun enableAddPaymentBtn(enable: Boolean) {
        if (enableAddPayment != enable){
            enableAddPayment = enable
            notifyItemChanged(0)
        }
    }

    class PaymentViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val t1 = view.findViewById<TextView>(R.id.text1)
        val t2 = view.findViewById<TextView>(R.id.text2)
        val t3 = view.findViewById<TextView>(R.id.text3)
        val trash = view.findViewById<View>(R.id.trash_item)
    }

    class HeaderViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val t2 = view.findViewById<TextView>(R.id.text2)
        val btn = view.findViewById<View>(R.id.add_payment)
    }

}

