package ch.swissbytes.syscomapppayment.fragments.payment.vms.models

import ch.swissbytes.syscomapppayment.AllPaymentsInterface
import java.util.*


class PaymentModel: Observable(), AllPaymentsInterface {

    companion object {
        val PAYMENT_TYPE = "paymentType"
        val TRANSACTION_BOB = "transactionBob"
        val TRANSACTION_USD = "transactionUsd"
        val MISSING_BOB = "missingBob"
        val MISSING_USD = "missingUsd"
        val CHANGE_CASH_TRANSACTION_BOB = "changeCashTransactionBob"
        val CHANGE_CASH_TRANSACTION_USD = "changeCashTransactionUsd"
        val MISSING_CHANGE_BOB = "missingChangeBob"
        val MISSING_CHANGE_USD = "missingChangeUsd"
        val CURRENCY = "currency"
        val AMOUNT_TO_PAY_IN_USD = "amountToPayInUsd"
        val DATE = "date"
        val TRANSACTION_NUMBER = "transactionNumber"
        val BANK_ID = "bankId"
        val BANK_NAME = "bankName"
    }

    override var transactionBob: String? = null
        set(value) {
            field = value
            setChangedAndNotify(TRANSACTION_BOB)
        }

    override var transactionUsd: String? = null
        set(value) {
            field = value
            setChangedAndNotify(TRANSACTION_USD)
        }

    override var missingBob: String? = null
        set(value) {
            field = value
            setChangedAndNotify(MISSING_BOB)
        }

    override var missingUsd: String? = null
        set(value) {
            field = value
            setChangedAndNotify(MISSING_USD)
        }

    override var changeCashTransactionBob: String? = null
        set(value) {
            field = value
            setChangedAndNotify(CHANGE_CASH_TRANSACTION_BOB)
        }

    override var changeCashTransactionUsd: String? = null
        set(value) {
            field = value
            setChangedAndNotify(CHANGE_CASH_TRANSACTION_USD)
        }

    override var missingChangeBob: String? = null
        set(value) {
            field = value
            setChangedAndNotify(MISSING_CHANGE_BOB)
        }

    override var missingChangeUsd: String? = null
        set(value) {
            field = value
            setChangedAndNotify(MISSING_CHANGE_USD)
        }

    override var currency: String? = null
        set(value) {
            field = value
            setChangedAndNotify(CURRENCY)
        }

    override var type: String? = null
        set(value) { field = value }

    override var amountToPayInUsd: String? = null
        set(value) {
            field = value
            setChangedAndNotify(AMOUNT_TO_PAY_IN_USD)
        }

    override var date: Long? = null
        set(value) {
            field = value
            setChangedAndNotify(DATE)
        }

    override var transactionNumber: String = ""
        set(value) {
            field = value
            setChangedAndNotify(TRANSACTION_NUMBER)
        }

    override var bankId: Long? = null
        set(value) {
            field = value
            setChangedAndNotify(BANK_ID)
        }

    override var bankName: String? = null
        set(value) {
            field = value
            setChangedAndNotify(BANK_NAME)
        }


    override var bankAccountId: Long? = null
        set(value) {
            field = value
            setChangedAndNotify(BANK_ID)
        }

    override val amountReceivedBob: String? = null
    override val amountReceivedUsd: String? = null
    override val differenceBob: String? = null

    private var updateUi: Boolean = true
    fun withoutUpdatingUi(func: () -> Unit){
        updateUi = false
        func.invoke()
        updateUi = true
    }


    override fun toString(): String = "amountToPayInUsd: $amountToPayInUsd, " +
                "currency: $currency" +
                "transactionBob: $transactionBob, " +
                "transactionUsd: $transactionUsd, " +
                "missingBob: $missingBob, " +
                "missingUsd: $missingUsd, " +
                "changeCashTransactionBob: $changeCashTransactionBob, " +
                "changeCashTransactionUsd: $changeCashTransactionUsd, " +
                "missingChangeBob: $missingChangeBob, " +
                "missingChangeUsd: $missingChangeUsd, " +
                "date: $date, " +
                "bankId: $bankId, " +
                "transactionNumber: $transactionNumber, "

    private fun setChangedAndNotify(field: String) {
        if (updateUi){
            setChanged()
            notifyObservers(field)
        }
    }
}