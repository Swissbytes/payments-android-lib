package ch.swissbytes.syscomapppayment.fragments.payment


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import ch.swissbytes.syscomappbase.extensions.replaceFragmentAllowingStateLoss
import ch.swissbytes.syscomappbase.extensions.zeroIfNull
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomapppayment.AllPaymentsInterface
import ch.swissbytes.syscomapppayment.BR
import ch.swissbytes.syscomapppayment.CashPaymentInterface
import ch.swissbytes.syscomapppayment.R
import ch.swissbytes.syscomapppayment.fragments.payment.cashcalculation.UpdatePaymentListener
import ch.swissbytes.syscomapppayment.fragments.payment.vms.CardPaymentVm
import ch.swissbytes.syscomapppayment.fragments.payment.vms.CashPaymentVm
import ch.swissbytes.syscomapppayment.fragments.payment.vms.DepositPaymentVm
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentCurrencies
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentModel
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentType
import java.math.BigDecimal


private const val ARG_PAYMENT_TYPE = "ARG_PAYMENT_TYPE"
private const val ARG_PAYMENT_AMOUNT = "ARG_PAYMENT_AMOUNT"
private const val ARG_IS_ADVANCE = "ARG_IS_ADVANCE"

/**
 * A fragment responsible in displaying the forms depending of the [PaymentType] selected by the user
 * Current [PaymentType] implemented [PaymentType.CASH] [PaymentType.ACCOUNT_DEPOSIT]
 *
 * @property onPaymentChange callback can return a [CashPaymentInterface] or [DepositCheckAndTransferencePaymentInterface
 * @property totalAmountToPayInUsd can be set dynamically to update the totalPaymentAmount to pay
 * @property currencyUs can be set dynamically to update the currency
 */
class PaymentFragment : Fragment() {

    private val TAG = this::class.java.simpleName
    private var listener: UpdatePaymentListener? = null

    var onPaymentChange: GenericListener<AllPaymentsInterface>? = null

    private var cashPaymentVm: CashPaymentVm? = null
    private var depositPaymentVm: DepositPaymentVm? = null
    private var cardPaymentVm: CardPaymentVm? = null

    private var amountUsdArg: String? = null
    private var typeArg: String? = null
    private var isAdvance: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            typeArg = it.getString(ARG_PAYMENT_TYPE)
            amountUsdArg = it.getString(ARG_PAYMENT_AMOUNT)
            isAdvance = it.getBoolean(ARG_IS_ADVANCE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        getVmBinding(inflater, container)

    /**
     * Allow changing the fragment displayed depending of the typeArg parameter
     *
     * @param type The [PaymentType] fragment to display
     * @param totalPaymentUsd The totalPaymentAmount to pay
     *
     * @return the instance displayed [PaymentFragment]
     */
    fun replaceFragmentPaymentType(
        type: PaymentType = PaymentType.CASH,
        totalPaymentUsd: BigDecimal
    ): PaymentFragment {
        (activity as AppCompatActivity).apply {
            val fr = newInstance(type, totalPaymentUsd = totalPaymentUsd.toString(), isAdvance = isAdvance)
                .also { it.onPaymentChange = onPaymentChange }

            replaceFragmentAllowingStateLoss(fr, (view!!.parent as ViewGroup).id)
            return fr
        }
    }

    /**
     * Create the vm the fragment should display depending of the [PaymentType].
     * This is called from [PaymentFragment.onCreateView]
     */
    private fun getVmBinding(inflater: LayoutInflater, container: ViewGroup?): View? {
        val paymentModel = PaymentModel().apply apply@{ this@apply.type = typeArg }
        return when (PaymentType.valueOf(typeArg!!)) {

            /**
             * [CashPaymentVm]
             */
            PaymentType.CASH -> {
                cashPaymentVm = CashPaymentVm(paymentModel, isAdvance) {
                    onPaymentChange?.invoke(it)
                }.also{
                    it.amountToPayInUsd = amountUsdArg.zeroIfNull()
                }

                listener = cashPaymentVm

                val binding =
                    DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.fragment_payment_cash, container, false)
                binding.setVariable(BR.vm, cashPaymentVm)
                binding.root
            }

            /**
             * [DepositPaymentVm]
             */
            PaymentType.ACCOUNT_DEPOSIT, PaymentType.TRANSFER_ORDER, PaymentType.CHECK -> {
                depositPaymentVm = DepositPaymentVm(context!!, paymentModel) { onPaymentChange?.invoke(it) }
                    .also { it.amountToPayInUsd = amountUsdArg.zeroIfNull() }

                listener = depositPaymentVm

                val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.fragment_payment_deposit, container, false)
                binding.setVariable(BR.vm, depositPaymentVm)
                binding.root
            }

            /**
             * [CardPaymentVm]
             */
            PaymentType.CREDIT_CARD, PaymentType.DEBIT_CARD -> {
                cardPaymentVm = CardPaymentVm(context!!, paymentModel) { onPaymentChange?.invoke(it) }
                    .also { it.amountToPayInUsd = amountUsdArg.zeroIfNull() }

                listener = cardPaymentVm

                val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.fragment_payment_debit_credit_card, container, false)
                binding.setVariable(BR.vm, cardPaymentVm)
                binding.root
            }
        }
    }


    fun onCurrentPaymentAddedToAdapter(remainPaymentUsd: BigDecimal){
        cashPaymentVm?.clear(remainPaymentUsd)
        depositPaymentVm?.clear(remainPaymentUsd)
        cardPaymentVm?.clear(remainPaymentUsd)

        Log.i(TAG, "clear remainPaymentUsd $remainPaymentUsd")
    }

    fun onPaymentRemovedFromAdapter(remainPaymentUsd: BigDecimal){
        cashPaymentVm?.apply {
            amountToPayInUsd = remainPaymentUsd
        }

        depositPaymentVm?.apply {
            amountToPayInUsd = remainPaymentUsd
        }

        cardPaymentVm?.apply {
            amountToPayInUsd = remainPaymentUsd
        }
    }

    /**
     * Update the totalAmount to pay
     */
    var totalAmountToPayInUsd: BigDecimal? = null
        set(value) {
            field = value
            field?.let {
                listener?.amountToPayInUsd = it
            }
        }

    /**
     * Update the currency
     */
    var currencyUs: Boolean? = null
        set(value) {
            field = value
            field?.let {
                listener?.currency = if (it) PaymentCurrencies.USD else PaymentCurrencies.BOB
            }
        }

    override fun onDetach() {
        super.onDetach()
        listener = null
        cashPaymentVm?.onDestroy()
    }

    companion object {
        @JvmStatic fun newInstance(type: PaymentType = PaymentType.CASH, totalPaymentUsd: String, isAdvance: Boolean = false) =
            PaymentFragment().apply { arguments = Bundle().apply {
                putString(ARG_PAYMENT_AMOUNT, totalPaymentUsd)
                putBoolean(ARG_IS_ADVANCE, isAdvance)
                putString(ARG_PAYMENT_TYPE, type.toString())
            } }
    }

}




