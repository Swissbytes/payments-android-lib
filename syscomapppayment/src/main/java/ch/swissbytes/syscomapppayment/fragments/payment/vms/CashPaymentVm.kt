package ch.swissbytes.syscomapppayment.fragments.payment.vms

import android.util.Log
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import ch.swissbytes.syscomappbase.extensions.greaterThanZero
import ch.swissbytes.syscomappbase.extensions.toBigDecimalOrZero
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomapppayment.AllPaymentsInterface
import ch.swissbytes.syscomapppayment.BR
import ch.swissbytes.syscomapppayment.base.extensions.parsePaymentInput
import ch.swissbytes.syscomapppayment.base.extensions.parsePaymentInputToBigDecimal
import ch.swissbytes.syscomapppayment.currency.getGlobalCurrencyService
import ch.swissbytes.syscomapppayment.fragments.payment.cashcalculation.CashTransactionInput
import ch.swissbytes.syscomapppayment.fragments.payment.cashcalculation.CashTransactionOutput
import ch.swissbytes.syscomapppayment.fragments.payment.cashcalculation.PaymentService
import ch.swissbytes.syscomapppayment.fragments.payment.cashcalculation.UpdatePaymentListener
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentCurrencies
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentModel
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit

class CashPaymentVm(
    private var paymentM: PaymentModel,
    private var isAdvance: Boolean,
    private val paymentCashChangeListener: GenericListener<AllPaymentsInterface>? = null
): Observer, BaseObservable(), UpdatePaymentListener {

    private val TAG = this::class.java.simpleName

    private val paymentService: PaymentService by lazy {
        PaymentService(getGlobalCurrencyService())
    }

    private val onTypedAmountChangeSubject by lazy { BehaviorSubject.create<Boolean>() }
    private var disposables: MutableList<Disposable> = mutableListOf()

    init {
        paymentM.addObserver(this)
        val disposable = onTypedAmountChangeSubject
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribe {
                val input = userInput(fromCashTransactionInputChange = it)
                Log.i(TAG, "calculate input $input")
                paymentService.calculateChangeCashTransactionAsync(input) { result ->
                    parseOutput(result)
                    paymentCashChangeListener?.invoke(paymentM)
                }
            }

        disposables.add(disposable)
    }

    private fun userInput(fromCashTransactionInputChange:Boolean):CashTransactionInput =
        CashTransactionInput(
                triggerByCashTransactionInputChange = fromCashTransactionInputChange,
                transactionBob = transactionBob.parsedBigDecimal(),
                transactionUsd = transactionUsd.parsedBigDecimal(),
                changeCashTransactionBob = changeCashTransactionBob,
                changeCashTransactionUsd = changeCashTransactionUsd,
                amountToPayUsd = amountToPayInUsd,
                isAdvance = isAdvance
        )

    private fun String.parsedBigDecimal(): BigDecimal =
        this.removeSuffix(".").toBigDecimalOrZero()

    private fun parseOutput(output: CashTransactionOutput){
        Log.i(TAG, "calculate output: $output")

        calculatingChange = false

        val lShowMissingChangeBobField =
            output.missingChangeBob.greaterThanZero() || output.changeCashTransactionBob?.greaterThanZero() == true
        val lShowMissingChangeUsdField =
            output.missingChangeUsd.greaterThanZero() || output.changeCashTransactionUsd?.greaterThanZero() == true

        if (output.triggerByCashTransactionInputChange){ // auto fill cashTransaction editText field
            if (currency == PaymentCurrencies.BOB   && lShowMissingChangeBobField) {
                showMissingChangeFields =
                        /*If the user has entered an advance we prevent the ui to show the missingChangeBob*/
                        output.amountReceivedInBob.greaterThanZero()

                requiredChangeCashBob = output.requiredChangeCashBob
                updateMissingChange(output)
                return
            }
            if (currency == PaymentCurrencies.USD  && lShowMissingChangeUsdField) {
                showMissingChangeFields =
                        /* If the user has entered an advance we prevent the ui to show the missingChangeBob */
                    output.amountReceivedInBob.greaterThanZero()

                requiredChangeCashUsd = output.requiredChangeCashUsd

                updateMissingChange(output)
                return
            }
        }


        showMissingChangeFields = lShowMissingChangeBobField || lShowMissingChangeUsdField
                && /*If the user has entered an advance we prevent the ui to show the missingChangeBob*/
                output.amountReceivedInBob.greaterThanZero()

        if (!showMissingChangeFields) {
            requiredChangeCashBob = BigDecimal.ZERO
        }

//        showMissingChangeUsdField = lShowMissingChangeUsdField
//                && /* If the user has entered an advance we prevent the ui to show the missingChangeBob */
//                output.amountReceivedInBob.greaterThanZero()

        Log.i(TAG, "showMissingChangeFields: $showMissingChangeFields")

        Log.i(TAG, "paymentM: $paymentM")

        updateMissingChange(output)
    }

    private fun updateMissingChange(output: CashTransactionOutput){
        missingBob = output.missingBob
        missingUsd = output.missingUsd
        missingChangeBob = output.missingChangeBob
        missingChangeUsd = output.missingChangeUsd
        changeCashTransactionUsd = output.changeCashTransactionUsd ?: BigDecimal.ZERO
        changeCashTransactionBob = output.changeCashTransactionBob ?: BigDecimal.ZERO

    }

    fun onDestroy(){
        disposables.forEach { it.dispose() }
        paymentService.clear()
    }

    private fun calculate(fromCashTransactionInputChange: Boolean = false) {
        onTypedAmountChangeSubject.onNext(fromCashTransactionInputChange)
    }

    fun onTotalPaymentAmountChange(s: CharSequence, start: Int, before: Int, count: Int) {
        Log.w(TAG, "onTotalPaymentAmountChange $s")
//        totalPaymentAmountChangeSubject.onNext(NumberUtils.zeroIfNull(s.toString()))
        calculate()
    }

    fun onCashTransactionBobChange(s: CharSequence, start: Int, before: Int, count: Int) {
        Log.w(TAG, "onCashTransactionBobChange $s")
        s.parsePaymentInput { transactionBob = it }

        calculate(true)
    }

    fun onCashTransactionUsdChange(s: CharSequence, start: Int, before: Int, count: Int) {
        Log.w(TAG, "onCashTransactionUsdChange $s")
        s.parsePaymentInput { transactionUsd = it }

        calculate(true)
    }

    fun onChangeCashTransactionBobChange(s: CharSequence, start: Int, before: Int, count: Int) {
        s.parsePaymentInputToBigDecimal {
            changeCashTransactionBob = it
        }

        calculate(false)
    }

    fun onChangeCashTransactionUsdChange(s: CharSequence, start: Int, before: Int, count: Int) {
        s.parsePaymentInputToBigDecimal { changeCashTransactionUsd = it }

        calculate(false)
    }


    override fun update(o: Observable?, fieldChanged: Any?) {
        if (fieldChanged is String){
            when (fieldChanged){
                PaymentModel.TRANSACTION_BOB -> notifyPropertyChanged(BR.transactionBob)
                PaymentModel.TRANSACTION_USD -> notifyPropertyChanged(BR.transactionUsd)
                PaymentModel.CHANGE_CASH_TRANSACTION_BOB -> notifyPropertyChanged(BR.changeCashTransactionBob)
                PaymentModel.CHANGE_CASH_TRANSACTION_USD -> notifyPropertyChanged(BR.changeCashTransactionUsd)
                PaymentModel.MISSING_BOB -> notifyPropertyChanged(BR.missingBob)
                PaymentModel.MISSING_USD -> notifyPropertyChanged(BR.missingUsd)
                PaymentModel.MISSING_CHANGE_BOB -> notifyPropertyChanged(BR.missingChangeBob)
                PaymentModel.MISSING_CHANGE_USD -> notifyPropertyChanged(BR.missingChangeUsd)
                PaymentModel.AMOUNT_TO_PAY_IN_USD -> {
                    calculate()
                }
                PaymentModel.CURRENCY -> {
                    notifyPropertyChanged(BR.currency)
                    calculate()
                }
            }
        }
    }

    fun clear(remainPaymentUsd: BigDecimal) {
        warningMessage = ""
        transactionBob = BigDecimal.ZERO.toString()
        transactionUsd = BigDecimal.ZERO.toString()
        missingChangeBob = BigDecimal.ZERO
        missingChangeUsd = BigDecimal.ZERO
        changeCashTransactionBob = BigDecimal.ZERO
        changeCashTransactionUsd = BigDecimal.ZERO
        amountToPayInUsd = remainPaymentUsd
    }

    override var amountToPayInUsd: BigDecimal = BigDecimal.ZERO
        set(value) {
            field = value
            paymentM.amountToPayInUsd = field.toString()
        }

    var requiredChangeCashUsd: BigDecimal = BigDecimal.ZERO @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.requiredChangeCashUsd)
            changeCashTransactionUsd = value
        }

    var requiredChangeCashBob: BigDecimal = BigDecimal.ZERO @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.requiredChangeCashBob)
            changeCashTransactionBob = value
        }

    var showMissingChangeFields: Boolean = false @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.showMissingChangeFields)
        }

//    var showMissingChangeUsdField: Boolean = false @Bindable get() = field
//        set(value) {
//            field = value
//            notifyPropertyChanged(BR.showMissingChangeUsdField)
//        }

    override var currency: PaymentCurrencies = PaymentCurrencies.BOB @Bindable get() = field
        set(value) {
            field = value
            paymentM.currency = field.name
        }

    var transactionBob: String = BigDecimal.ZERO.toString() @Bindable get() = field
        set(value) {
            field = value
            paymentM.transactionBob = field
        }

    var transactionUsd: String = BigDecimal.ZERO.toString() @Bindable get() = paymentM.transactionUsd ?: "0"
        set(value) {
            field = value
            paymentM.transactionUsd = field
        }

    var changeCashTransactionBob: BigDecimal = BigDecimal.ZERO @Bindable get() = paymentM.changeCashTransactionBob.toBigDecimalOrZero()
        set(value) {
            field = value
            paymentM.changeCashTransactionBob = field.toString()
        }

    var changeCashTransactionUsd: BigDecimal = BigDecimal.ZERO @Bindable get() = paymentM.changeCashTransactionUsd.toBigDecimalOrZero()
        set(value) {
            field = value
            paymentM.changeCashTransactionUsd = field.toString()
        }

    var missingBob: BigDecimal = BigDecimal.ZERO @Bindable get() = paymentM.missingBob.toBigDecimalOrZero()
        set(value) {
            field = value
            paymentM.missingBob = field.toString()
        }

    var missingUsd: BigDecimal = BigDecimal.ZERO @Bindable get() = paymentM.missingUsd.toBigDecimalOrZero()
        set(value) {
            field = value
            paymentM.missingUsd = field.toString()
        }

    var missingChangeBob: BigDecimal = BigDecimal.ZERO @Bindable get() = paymentM.missingChangeBob.toBigDecimalOrZero()
        set(value) {
            field = value
            paymentM.missingChangeBob = field.toString()
        }

    var missingChangeUsd: BigDecimal = BigDecimal.ZERO @Bindable get() = paymentM.missingChangeUsd.toBigDecimalOrZero()
        set(value) {
            field = value
            paymentM.missingChangeUsd = field.toString()
        }

    var calculatingChange = false @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.calculatingChange)
        }

    var warningMessage = "" @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.warningMessage)
        }


}