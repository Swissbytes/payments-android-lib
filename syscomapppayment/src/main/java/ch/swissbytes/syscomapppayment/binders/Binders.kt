package ch.swissbytes.syscomapppayment.binders

import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import ch.swissbytes.syscomapppayment.R


@BindingAdapter("items")
fun spinner(sp: Spinner, list: List<String>) {
    sp.adapter = ArrayAdapter(sp.context, R.layout.spinner_adapter_item, list);
}
