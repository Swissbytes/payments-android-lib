package ch.swissbytes.syscomapppayment

import android.util.Log
import ch.swissbytes.syscomappbase.extensions.toBigDecimalOrZero
import ch.swissbytes.syscomappbase.extensions.toFormatDecimalDefault
import ch.swissbytes.syscomapppayment.currency.CurrencyService
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentCurrencies
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentType

interface CardPaymentInterface {
    val transactionNumber: String?
}

interface CashPaymentInterface {
    val changeCashTransactionBob: String?
    val changeCashTransactionUsd: String?
    val missingChangeBob: String?
    val missingChangeUsd: String?
}

interface DepositCheckAndTransferencePaymentInterface {
    val date: Long?
    val transactionNumber: String?
    val bankId: Long?
    val bankAccountId: Long?
    val bankName: String?
}


interface AllPaymentsInterface : CardPaymentInterface, CashPaymentInterface, DepositCheckAndTransferencePaymentInterface{
    val amountToPayInUsd: String?
    val transactionBob: String?
    val transactionUsd: String?
    val missingBob: String?
    val missingUsd: String?
    val type: String?
    val currency: String?

    val amountReceivedBob: String?
    val amountReceivedUsd: String?

    val differenceBob: String?
}

class AllPaymentsImpl(c: CurrencyService): AllPaymentsInterface {

    constructor(c: CurrencyService, int: AllPaymentsInterface): this(c){
        amountToPayInUsd = int.amountToPayInUsd
        currency = int.currency
        type = int.type
        transactionBob = int.transactionBob
        transactionUsd = int.transactionUsd
        missingBob = int.missingBob
        missingUsd = int.missingUsd
        changeCashTransactionBob = int.changeCashTransactionBob
        changeCashTransactionUsd = int.changeCashTransactionUsd
        missingChangeBob = int.missingChangeBob
        missingChangeUsd = int.missingChangeUsd
        date = int.date
        transactionNumber = int.transactionNumber
        bankId = int.bankId
        bankAccountId = int.bankAccountId
        bankName = int.bankName

        updateAmountReceivedBob(c)
        Log.i("TAG", "AllPaymentsImpl amountReceivedBob: $amountReceivedBob")
    }

    override var amountToPayInUsd: String? = null
    override var currency: String? = null
    override var type: String? = null
    override var transactionBob: String? = null
    override var transactionUsd: String? = null
    override var missingBob: String? = null
    override var missingUsd: String? = null
    override var changeCashTransactionBob: String? = null
    override var changeCashTransactionUsd: String? = null
    override var missingChangeBob: String? = null
    override var missingChangeUsd: String? = null
    override var date: Long? = null
    override var transactionNumber: String? = ""
    override var bankId: Long? = null
    override var bankAccountId: Long? = null
    override var bankName: String? = null

    override var amountReceivedBob: String? = null
    override var amountReceivedUsd: String? = null

    override var differenceBob: String? = null

    override fun toString(): String =
                "AllPaymentsImpl(" +
                "amountToPayInUsd=$amountToPayInUsd, " +
                "currency=$currency, " +
                "type=$type, " +
                "transactionBob=$transactionBob, " +
                "transactionUsd=$transactionUsd, " +
                "missingBob=$missingBob, " +
                "missingUsd=$missingUsd, " +
                "changeCashTransactionBob=$changeCashTransactionBob, " +
                "changeCashTransactionUsd=$changeCashTransactionUsd, " +
                "missingChangeBob=$missingChangeBob, " +
                "missingChangeUsd=$missingChangeUsd, " +
                "date=$date, " +
                "transactionNumber=$transactionNumber, " +
                "bankId=$bankId, " +
                "bankName=$bankName, " +
                "amountReceivedUsd=$amountReceivedUsd, " +
                "amountReceivedBob=$amountReceivedBob)"

}

fun AllPaymentsImpl.updateAmountReceivedBob(c: CurrencyService) {
    amountReceivedBob = when (PaymentType.valueOf(type!!)){
        PaymentType.CASH ->
            transactionBob.toBigDecimalOrZero()
                .add(c.USDtoBOB(transactionUsd.toBigDecimalOrZero()))
                .minus(changeCashTransactionBob.toBigDecimalOrZero().add(c.USDtoBOB(changeCashTransactionUsd.toBigDecimalOrZero())))

        PaymentType.ACCOUNT_DEPOSIT, PaymentType.TRANSFER_ORDER, PaymentType.CHECK, PaymentType.CREDIT_CARD, PaymentType.DEBIT_CARD ->
            if (currency == PaymentCurrencies.BOB.name) transactionBob.toBigDecimalOrZero() else c.USDtoBOB(transactionUsd.toBigDecimalOrZero())
    }.toFormatDecimalDefault()

    amountReceivedUsd = c.BOBtoUSD(amountReceivedBob.toBigDecimalOrZero()).toFormatDecimalDefault()
}
