package ch.swissbytes.syscomapppayment.realm

import ch.swissbytes.syscomapppayment.realm.entities.EntityStatus
import ch.swissbytes.syscomapppayment.realm.module.RealmPaymentModule
import io.realm.DynamicRealm
import io.realm.RealmConfiguration
import io.realm.RealmMigration

private fun realmMigration() : RealmMigration = object: RealmMigration {
    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        var oVersion = oldVersion.toInt()
        val schema = realm.schema
            if (oVersion == 0) {
                schema.get("BankAccountRealm")
                    ?.addField("tmp", String::class.java)
                    ?.transform {
                        if (it.isNull("status")){
                            it.setString("tmp", EntityStatus.ENABLE.name)
                        } else {
                            val oldValue = it.getLong("status")
                            it.setString("tmp", EntityStatus.values()[oldValue.toInt()].name)
                        }
                    }
                    ?.removeField("status")
                    ?.renameField("tmp", "status")

                schema.create("BankRealm")
                    .addField("id", Long::class.java)
                    .setNullable("id", true)
                    .addPrimaryKey("id")
                    .addField("bankName", String::class.java)
                    .addField("lastUpdate", Long::class.java)
                    .setNullable("lastUpdate", true)
            }
    }
}

class RealmPaymentConf  {
   companion object {
       val config: RealmConfiguration by lazy {
           RealmConfiguration.Builder()
               .name("ch.swissbytes.syscomapppayment.realm")
               .modules(RealmPaymentModule())
//               .deleteRealmIfMigrationNeeded()
               .schemaVersion(1)
               .migration(realmMigration())
               .build()
       }
   }
}