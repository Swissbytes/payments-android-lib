package ch.swissbytes.syscomapppayment.realm.entities

import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass


interface BankInterfaceRealm: RealmModel, BankInterface

interface BankInterface {
    var id: Long?
    var bankName: String?
}

@RealmClass
open class BankAccountRealm : BankInterfaceRealm {
    @PrimaryKey override var id: Long? = null
    var number: String? = null
    var currency: String? = null
    override var bankName: String? = null
    var status: String? = null
    var lastUpdate: Long? = null
}

@RealmClass
open class BankRealm : BankInterfaceRealm  {
    @PrimaryKey override var id: Long? = null
    override var bankName: String? = null
    var lastUpdate: Long? = null
}

@RealmClass
open class CurrencyRealm: RealmModel {
    @PrimaryKey var id: Long = 0L
    var quote: String? = null
}

@RealmClass
open class PermittedDiffRealm: RealmModel {
    @PrimaryKey var id: Long = 0L
    var diffAllowed: String? = null
}

enum class EntityStatus{ ENABLE, DISABLE }
