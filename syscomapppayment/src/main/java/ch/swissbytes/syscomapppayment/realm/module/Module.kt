package ch.swissbytes.syscomapppayment.realm.module
import io.realm.annotations.RealmModule

@RealmModule(library = true, allClasses = true)
class RealmPaymentModule