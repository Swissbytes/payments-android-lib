package ch.swissbytes.syscomapppayment.realm
import io.realm.Realm

class RealmPayment{
    companion object {
       @JvmStatic fun getDefaultInstance() = Realm.getInstance(RealmPaymentConf.config)
    }
}