package ch.swissbytes.syscomapppayment.services

import ch.swissbytes.syscomappbase.appspecific.SyncInterface
import ch.swissbytes.syscomappbase.appspecific.hasCollectorRole
import ch.swissbytes.syscomappbase.appspecific.hasFleetRole
import ch.swissbytes.syscomappbase.interfaces.SyscomBaseSync
import ch.swissbytes.syscomapppayment.api.RestPayment
import ch.swissbytes.syscomapppayment.base.extensions.realmPersist
import ch.swissbytes.syscomapppayment.realm.RealmPayment
import io.reactivex.Observable
import retrofit2.Response



class PaymentSync(
    private val input: SyncInterface
): SyscomBaseSync() {

    private val rest: RestPayment = input.run { RestPayment.create(url, token, versionCode, appId, lastUpdate) }

    override val isDebug: Boolean
        get() = input.debug

    override fun onResponse(model: Any) {
        realmPersist(model, realm = RealmPayment.getDefaultInstance())
    }

    override val calls: Map<String, Observable<out Response<out Any>>> by lazy {
        if (!input.roles.hasCollectorRole() && !input.roles.hasFleetRole()){
            // The user is only seller
            emptyMap()
        } else {
            mapOf(
                "banks" to rest.banks(),
                "banksAccount" to rest.banksAccount(),
                "currency" to rest.currency(),
                "collectionPermittedDiff" to rest.collectionPermittedDiff()
            )
        }
    }

}
