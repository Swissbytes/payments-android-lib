package ch.swissbytes.syscomapppayment.api

import ch.swissbytes.syscomapppayment.realm.entities.BankAccountRealm
import ch.swissbytes.syscomapppayment.realm.entities.BankRealm
import ch.swissbytes.syscomapppayment.realm.entities.CurrencyRealm
import ch.swissbytes.syscomapppayment.realm.entities.PermittedDiffRealm
import io.reactivex.Observable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.io.IOException
import javax.annotation.ParametersAreNonnullByDefault


interface RestPayment {

    @GET("shared/bank-account/list")
    fun banksAccount(): Observable<Response<List<BankAccountRealm>>>

    @GET("mobilestore/exchangeRate")
    fun currency(): Observable<Response<CurrencyRealm>>

    @GET("configuration/collectionPermittedDiff")
    fun collectionPermittedDiff(): Observable<Response<PermittedDiffRealm>>

    @GET("shared/banks/list")
    fun banks(): Observable<Response<List<BankRealm>>>

    companion object {
        fun create(
            url:String,
            token: String,
            versionCode: String,
            appId: String,
            lastUpdate: Long? = null
        ): RestPayment {

            val client = OkHttpClient().newBuilder()
            client.interceptors().add(HeaderInterceptor(token, versionCode, appId, lastUpdate))
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client.build())
                    .baseUrl(url)
                    .build()

            return retrofit.create(RestPayment::class.java)
        }
    }

}

class HeaderInterceptor(
    private val token: String,
    private val versionCode: String,
    private val appId: String,
    private val lastUpdate: Long?
) : Interceptor {
    private val AUTHORIZATION = "Authorization"
    private val BUILD = "versionAppBuild"
    private val CODE = "versionAppCode"
    private val LAST_UPDATE = "lastUpdate"

    @ParametersAreNonnullByDefault
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        val requestBuilder = chain.request().newBuilder()
        requestBuilder.addHeader(AUTHORIZATION, "Bearer $token")
        requestBuilder.addHeader(BUILD, versionCode)
        requestBuilder.addHeader(CODE, appId)
        requestBuilder.addHeader(LAST_UPDATE, "${lastUpdate ?: 0}")
        return chain.proceed(requestBuilder.build())
    }
}