package ch.swissbytes.syscomapppaymentexample

import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import ch.swissbytes.syscomappbase.extensions.addFragment
import ch.swissbytes.syscomappbase.extensions.greaterThanZero
import ch.swissbytes.syscomappbase.extensions.showToast
import ch.swissbytes.syscomapppayment.AllPaymentsInterface
import ch.swissbytes.syscomapppayment.base.extensions.realmTransaction
import ch.swissbytes.syscomapppayment.currency.getGlobalCurrencyService
import ch.swissbytes.syscomapppayment.fragments.PaymentTabFragment
import ch.swissbytes.syscomapppayment.fragments.payment.PaymentFragment
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentCurrencies
import ch.swissbytes.syscomapppayment.fragments.payment.vms.models.PaymentType
import ch.swissbytes.syscomapppayment.realm.RealmPayment
import ch.swissbytes.syscomapppayment.realm.entities.BankAccountRealm
import ch.swissbytes.syscomapppayment.realm.entities.BankRealm

class MainActivity : AppCompatActivity(), PaymentTabFragment.OnPayment {

    private val TAG = this::class.java.simpleName
    var pFragment: PaymentFragment? = null
    var show = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        realmTransaction(RealmPayment.getDefaultInstance()) {
            copyToRealmOrUpdate(BankAccountRealm().apply {
                id = 1
                number = "A number"
                currency = PaymentCurrencies.BOB.name
                bankName = "This a BOB bankAccount"
                status = "ENABLE"
            })

            copyToRealmOrUpdate(BankAccountRealm().apply {
                id = 2
                number = "A number"
                currency = PaymentCurrencies.USD.name
                bankName = "This a USD bankAccount"
                status = "ENABLE"
            })

            copyToRealmOrUpdate(BankRealm().apply {
                id = 3
                bankName = "This a bank"
            })

            copyToRealmOrUpdate(BankRealm().apply {
                id = 4
                bankName = "This a another bank"
            })
        }

//        InkDialog(this).showDialog {  }

        val totalAmountToPayUsd = "1.3062"
        val fragment = PaymentTabFragment
            .newInstance(
                PaymentType.CASH,
                totalAmountToPayInUsd = totalAmountToPayUsd,
                multiPayment = true
            )

        fragment.apply {
            onPaymentChange = { (totalAmountBs, _) ->
                Log.i(TAG, "onPaymentChange: $totalAmountBs")

//                val totalTransactionBob = payments.map { p -> p.transactionBob }
//                val totalTransactionUsd = payments.map { p -> p.transactionBob }

                val currencyService = getGlobalCurrencyService()
                val advanceAmountInBs = totalAmountBs.subtract(currencyService.USDtoBOB(totalAmountToPayUsd.toBigDecimal()))
                val advanceAmountInUsd = currencyService.BOBtoUSD(advanceAmountInBs)

                if (advanceAmountInBs.greaterThanZero()){
                    Log.i(TAG, "advanceAmountInBs: $advanceAmountInBs")
                    showToast("advanceAmountInBs: $advanceAmountInBs")
                }
            }
            beforePaymentConfirm = { then ->
                then.invoke()
            }
        }
        addFragment(fragment, R.id.fragment)
    }

    override fun onPaymentConfirm(signature: Bitmap?, payments: List<AllPaymentsInterface>?) {

    }
}
