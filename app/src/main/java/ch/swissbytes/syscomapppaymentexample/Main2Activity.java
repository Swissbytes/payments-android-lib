package ch.swissbytes.syscomapppaymentexample;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import androidx.appcompat.app.AppCompatActivity;

public class Main2Activity extends AppCompatActivity {

    private Intent _intent;
    private boolean isBound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _intent = new Intent(this, SyncService.class);
        startService(_intent);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!isBound){
            bindService(_intent, connection, BIND_AUTO_CREATE);
        }
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isBound){
            unbindService(connection);
        }
    }

    ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            SyncService.LocalBinder service1 = (SyncService.LocalBinder) service;
            _onServiceConnected(service1.getService());
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };


    private void _onServiceConnected(SyncService service){
        service.sync();
    }
}
