package ch.swissbytes.syscomapppaymentexample

import android.app.Service
import android.content.Intent
import android.os.Binder
import ch.swissbytes.syscomappbase.appspecific.Roles
import ch.swissbytes.syscomappbase.appspecific.SyncInterface
import ch.swissbytes.syscomappbase.interfaces.SyscomSyncInterface
import ch.swissbytes.syscomapppayment.services.PaymentSync
import java.util.*


class SyncService: Service() {

    private val TAG = this::class.java.simpleName
    override fun onBind(intent: Intent) = LocalBinder()
    inner class LocalBinder : Binder() {  val service = this@SyncService }
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int  = START_STICKY

    var callsDone: Int = 0
    var callsSize: Int = 0
    var errorOccure: Boolean = false

    var calls: List<SyscomSyncInterface> = emptyList(); get() =
        listOfNotNull(PaymentSync(
            object : SyncInterface {
                override var appId: String = ""
                override var url: String = "http://192.168.0.179:5080/rest/mobile/"
                override var token: String = ""
                override var versionCode: String = ""
                override var timeout: Long = 0
                override var lastUpdate: Long? = null
                override var roles: EnumSet<Roles> = EnumSet.of(Roles.SELLER, Roles.COLLECTOR, Roles.FLEET)
                override var debug: Boolean = true
            }
        )) //todo token!!

    fun sync(){
        if (calls.isEmpty()) this.stopSelf()
        else {
            calls.forEach { callsSize += it.numOfCalls() }
            calls.forEach { call -> call.startSync({ checkAdvance() }) { errorOccure = true } }
        }
    }

    @Synchronized fun checkAdvance(){
        if (callsSize == ++callsDone){
            this.stopSelf()
        }
    }

}
